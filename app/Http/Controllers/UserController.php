<?php

namespace adwords\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use adwords\User;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user/view');
    }

    /**
     * Add New User View Page
     */
    public function add() {
        return view('user/add');
    }

    /**
     * Add New User Process
     */
    public function addProcess(Request $request) {
        try{
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'role' => $request->role,
                'created_at' => date('Y-m-d H:i:s')
            ]);
            if(!empty($user)) {
                echo 'success';
            }
        } catch(Exception $e) {
            print_r($e);
        }
    }
}
