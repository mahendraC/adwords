$(document).ready(function(){
    $('#addUserForm').validate({ // initialize the plugin
        rules: {
            name: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 5
            },
            role: {
                required: true
            }
        }
    });
});