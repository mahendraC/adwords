@include('includes/header')
    <main class="animated">
      <ul class="dropdown-content action-ex-opts" id="generalDropDown">
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">account_box</i><span>Account</span></a></li>
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">local_activity</i><span>Recent Activities</span></a></li>
        <li><a class="waves-effect waves-set" href="#"><i class="mdi mdi-wheelchair-accessibility"></i><span>Accessibility</span></a></li>
        <li class="divider"></li>
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">settings</i><span>All Settings</span></a></li>
      </ul>
      <ul class="dropdown-content action-ex-opts" id="marketingCompaign">
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">account_box</i><span>Account</span></a></li>
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">local_activity</i><span>Recent Activities</span></a></li>
        <li><a class="waves-effect waves-set" href="#"><i class="mdi mdi-wheelchair-accessibility"></i><span>Accessibility</span></a></li>
        <li class="divider"></li>
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">settings</i><span>All Settings</span></a></li>
      </ul>
      <ul class="dropdown-content action-ex-opts" id="marketingCompaignInternal">
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">account_box</i><span>Account</span></a></li>
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">local_activity</i><span>Recent Activities</span></a></li>
        <li><a class="waves-effect waves-set" href="#"><i class="mdi mdi-wheelchair-accessibility"></i><span>Accessibility</span></a></li>
        <li class="divider"></li>
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">settings</i><span>All Settings</span></a></li>
      </ul>
      <ul class="dropdown-content action-ex-opts" id="supportTicket">
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">account_box</i><span>Account</span></a></li>
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">local_activity</i><span>Recent Activities</span></a></li>
        <li><a class="waves-effect waves-set" href="#"><i class="mdi mdi-wheelchair-accessibility"></i><span>Accessibility</span></a></li>
        <li class="divider"></li>
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">settings</i><span>All Settings</span></a></li>
      </ul>
      <ul class="dropdown-content action-ex-opts" id="supportTicketInternal">
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">account_box</i><span>Account</span></a></li>
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">local_activity</i><span>Recent Activities</span></a></li>
        <li><a class="waves-effect waves-set" href="#"><i class="mdi mdi-wheelchair-accessibility"></i><span>Accessibility</span></a></li>
        <li class="divider"></li>
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">settings</i><span>All Settings</span></a></li>
      </ul>
      <div class="main-header">
        <div class="sec-page">
          <div class="page-title">
            <h2>Dashboard</h2>
          </div>
          <div class="page-options"><a class="dropdown-button page-opt-dropBtn btn-floating waves-effect waves-set setWave" href="#" data-activates="generalDropDown" data-beloworigin="true" data-alignment="right" data-position="bottom" data-constrainwidth="false" data-delay="50" data-gutter="25"><i class="material-icons">perm_data_setting</i></a><a class="waves-effect waves-set page-opt-dropBtn setWave btn-floating" href="#"><i class="material-icons">chat_bubble_outline</i></a></div>
        </div>
      </div>
      <div class="main-container">
        <div class="row" style="margin-top:10px !important;">
          <div class="col s12 m12 l6">
            <div class="card card-dash card-stats">
              <div class="card-header primary-bg dark z-depth-2"><i class="material-icons">trending_up</i>
              </div>
              <div class="card-content">
                <h5 class="title grey-text text-darken-2">Popularity</h5>
                <p class="category">128 Sales/day</p>
              </div>
              <div class="card-content pos-relative">
                <canvas id="salesVisit" height="180"></canvas><a class="btn-floating activator halfway-fab waves-effect waves-light primary-bg dark" href="javascript:void(0)"><i class="material-icons">add</i></a>
              </div>
              <div class="card-action"><a class="primary-text dark" href="#"><i class="material-icons left">trending_up</i>Increment in sales</a></div>
              <div class="card-reveal"><span class="card-title grey-text text-darken-4">Sale & Earnings<i class="material-icons right">close</i></span>
                <table class="grey-text">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Day</th>
                      <th>Sale</th>
                      <th>Earned</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>Monday</td>
                      <td>35</td>
                      <td>$560</td>
                    </tr>
                    <tr>
                      <td>2</td>
                      <td>Tuesday</td>
                      <td>56</td>
                      <td>$896</td>
                    </tr>
                    <tr>
                      <td>3</td>
                      <td>Wednesday</td>
                      <td>88</td>
                      <td>$1480</td>
                    </tr>
                    <tr>
                      <td>4</td>
                      <td>Thurday</td>
                      <td>105</td>
                      <td>$1976</td>
                    </tr>
                    <tr>
                      <td>5</td>
                      <td>Friday</td>
                      <td>77</td>
                      <td>$1540</td>
                    </tr>
                    <tr>
                      <td>6</td>
                      <td>Saturday</td>
                      <td>49</td>
                      <td>$980</td>
                    </tr>
                    <tr>
                      <td>7</td>
                      <td>Sunday</td>
                      <td>68</td>
                      <td>$1496</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="col s12 m12 l6">
            <div class="card card-dash card-stats">
              <div class="card-header secondary-bg z-depth-2"><i class="material-icons">traffic</i>
              </div>
              <div class="card-content">
                <h5 class="title grey-text text-darken-2">Traffic</h5>
                <p class="category">20K/day </p>
              </div>
              <div class="card-content pos-relative">
                <canvas id="trafficSource" height="180"></canvas><a class="btn-floating activator halfway-fab waves-effect waves-light secondary-bg" href="javascript:void(0)"><i class="material-icons">add</i></a>
              </div>
              <div class="card-action"><a class="secondary-text" href="#"><i class="material-icons left">lightbulb_outline</i>Pupularity increased by 40%</a></div>
              <div class="card-reveal"><span class="card-title grey-text text-darken-4">Traffic<i class="material-icons right">close</i></span>
                <table class="grey-text">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Day</th>
                      <th>New Users</th>
                      <th>Session</th>
                      <th>Online</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>Monday</td>
                      <td>38</td>
                      <td>38</td>
                      <td>38</td>
                    </tr>
                    <tr>
                      <td>2</td>
                      <td>Tuesday</td>
                      <td>127</td>
                      <td>354</td>
                      <td>234</td>
                    </tr>
                    <tr>
                      <td>3</td>
                      <td>Wednesday</td>
                      <td>123</td>
                      <td>238</td>
                      <td>348</td>
                    </tr>
                    <tr>
                      <td>4</td>
                      <td>Thurday</td>
                      <td>271</td>
                      <td>345</td>
                      <td>356</td>
                    </tr>
                    <tr>
                      <td>5</td>
                      <td>Friday</td>
                      <td>244</td>
                      <td>434</td>
                      <td>249</td>
                    </tr>
                    <tr>
                      <td>6</td>
                      <td>Saturday</td>
                      <td>467</td>
                      <td>1234</td>
                      <td>1239</td>
                    </tr>
                    <tr>
                      <td>7</td>
                      <td>Sunday</td>
                      <td>239</td>
                      <td>748</td>
                      <td>1090</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col s12 m12 l8">
            <div class="card">
              <div class="card-content">
                <div class="row no-mrpd">
                  <div class="col s12 m8"><span class="card-title">Marketing Campaign</span></div>
                  <div class="col s12 m4 right-align hide-on-small-only"><a class="dropdown-button page-opt-dropBtn btn-floating btn-flat waves-effect waves-set setWave" href="javascript:void(0)" data-activates="marketingCompaign" data-beloworigin="true" data-alignment="right" data-position="bottom" data-constrainwidth="false" data-delay="50" data-gutter="25"><i class="material-icons grey-text text-darken-2">menu</i></a></div>
                </div>
                <table class="responsive-table statistic-table">
                  <thead>
                    <tr>
                      <th>Campaign</th>
                      <th>Client</th>
                      <th>Changes</th>
                      <th>Budget</th>
                      <th>Status</th>
                      <th class="expand_more center-align hide-on-small-only"><i class="material-icons">expand_more</i>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        <div class="left"><a class="btn-floating waves-effect waves-set setWave blue darken-4" href="#"><i class="mdi mdi-facebook"></i></a></div>
                        <div class="left pad-left-8">
                          <p>Facebook</p>
                          <p class="no-mrpd"><small>02:00 - 03:00</small></p>
                        </div>
                      </td>
                      <td class="client-name">Matthew Melifont</td>
                      <td class="green-text"><i class="material-icons left">trending_up</i>2.22%
                      </td>
                      <td><strong>$4423</strong></td>
                      <td class="green-text">Active</td>
                      <td class="center-align hide-on-small-only"><a class="dropdown-button page-opt-dropBtn btn-floating btn-flat waves-effect waves-set setWave" href="javascript:void(0)" data-activates="marketingCompaign" data-beloworigin="true" data-alignment="right" data-position="bottom" data-constrainwidth="false" data-delay="50" data-gutter="25"><i class="material-icons grey-text text-darken-2">more_vert</i></a></td>
                    </tr>
                    <tr>
                      <td>
                        <div class="left"><a class="btn-floating waves-effect waves-set setWave light-blue" href="#"><i class="mdi mdi-twitter"></i></a></div>
                        <div class="left pad-left-8">
                          <p>Twitter</p>
                          <p class="no-mrpd"><small>03:00 - 04:00</small></p>
                        </div>
                      </td>
                      <td class="client-name">Sarah Willson</td>
                      <td class="blue-text"><i class="material-icons left">trending_flat</i>2.22%
                      </td>
                      <td><strong>$2345</strong></td>
                      <td class="grey-text">Hold</td>
                      <td class="center-align hide-on-small-only"><a class="dropdown-button page-opt-dropBtn btn-floating btn-flat waves-effect waves-set setWave" href="javascript:void(0)" data-activates="marketingCompaign" data-beloworigin="true" data-alignment="right" data-position="bottom" data-constrainwidth="false" data-delay="50" data-gutter="25"><i class="material-icons grey-text text-darken-2">more_vert</i></a></td>
                    </tr>
                    <tr>
                      <td>
                        <div class="left"><a class="btn-floating waves-effect waves-set setWave red accent-4" href="#"><i class="mdi mdi-youtube-play"></i></a></div>
                        <div class="left pad-left-8">
                          <p>YouTube</p>
                          <p class="no-mrpd"><small>04:00 - 05:00</small></p>
                        </div>
                      </td>
                      <td class="client-name">Samantana Cruiz</td>
                      <td class="red-text"><i class="material-icons left">trending_down</i>2.22%
                      </td>
                      <td><strong>$1234</strong></td>
                      <td class="red-text">Closed</td>
                      <td class="center-align hide-on-small-only"><a class="dropdown-button page-opt-dropBtn btn-floating btn-flat waves-effect waves-set setWave" href="javascript:void(0)" data-activates="marketingCompaign" data-beloworigin="true" data-alignment="right" data-position="bottom" data-constrainwidth="false" data-delay="50" data-gutter="25"><i class="material-icons grey-text text-darken-2">more_vert</i></a></td>
                    </tr>
                    <tr>
                      <td>
                        <div class="left"><a class="btn-floating waves-effect waves-set setWave green accent-4" href="#"><i class="mdi mdi-spotify"></i></a></div>
                        <div class="left pad-left-8">
                          <p>Spotify</p>
                          <p class="no-mrpd"><small>05:00 - 06:00</small></p>
                        </div>
                      </td>
                      <td class="client-name">Jemmy Desuza</td>
                      <td class="green-text"><i class="material-icons left">trending_up</i>2.22%
                      </td>
                      <td><strong>$6554</strong></td>
                      <td class="green-text">Active</td>
                      <td class="center-align hide-on-small-only"><a class="dropdown-button page-opt-dropBtn btn-floating btn-flat waves-effect waves-set setWave" href="javascript:void(0)" data-activates="marketingCompaign" data-beloworigin="true" data-alignment="right" data-position="bottom" data-constrainwidth="false" data-delay="50" data-gutter="25"><i class="material-icons grey-text text-darken-2">more_vert</i></a></td>
                    </tr>
                    <tr class="grey lighten-3 hide-on-small-only">
                      <td> Yesterday</td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td class="center-align"><span class="taskblue25"></span></td>
                    </tr>
                    <tr>
                      <td>
                        <div class="left"><a class="btn-floating waves-effect waves-set setWave grey darken-4" href="#"><i class="mdi mdi-amazon"></i></a></div>
                        <div class="left pad-left-8">
                          <p>Amazon</p>
                          <p class="no-mrpd"><small>03:00 - 04:00</small></p>
                        </div>
                      </td>
                      <td class="client-name">Arash Tim</td>
                      <td class="green-text"><i class="material-icons left">trending_up</i>2.25%
                      </td>
                      <td><strong>$1237</strong></td>
                      <td class="amber-text">Pending</td>
                      <td class="center-align hide-on-small-only"><a class="dropdown-button page-opt-dropBtn btn-floating btn-flat waves-effect waves-set setWave" href="javascript:void(0)" data-activates="marketingCompaign" data-beloworigin="true" data-alignment="right" data-position="bottom" data-constrainwidth="false" data-delay="50" data-gutter="25"><i class="material-icons grey-text text-darken-2">more_vert</i></a></td>
                    </tr>
                    <tr>
                      <td>
                        <div class="left"><a class="btn-floating waves-effect waves-set setWave red accent-4" href="#"><i class="mdi mdi-pinterest"></i></a></div>
                        <div class="left pad-left-8">
                          <p>Pinterest</p>
                          <p class="no-mrpd"><small>04:00 - 05:00</small></p>
                        </div>
                      </td>
                      <td class="client-name">Willson Lou</td>
                      <td class="amber-text"><i class="material-icons left">trending_flat</i>4.23%
                      </td>
                      <td><strong>$2666</strong></td>
                      <td class="red-text">Closed</td>
                      <td class="center-align hide-on-small-only"><a class="dropdown-button page-opt-dropBtn btn-floating btn-flat waves-effect waves-set setWave" href="javascript:void(0)" data-activates="marketingCompaign" data-beloworigin="true" data-alignment="right" data-position="bottom" data-constrainwidth="false" data-delay="50" data-gutter="25"><i class="material-icons grey-text text-darken-2">more_vert</i></a></td>
                    </tr>
                    <tr>
                      <td>
                        <div class="left"><a class="btn-floating waves-effect waves-set setWave pink accent-4" href="#"><i class="mdi mdi-dribbble"></i></a></div>
                        <div class="left pad-left-8">
                          <p>Dribbble</p>
                          <p class="no-mrpd"><small>05:00 - 06:00</small></p>
                        </div>
                      </td>
                      <td class="client-name">Kelly</td>
                      <td class="green-text"><i class="material-icons left">trending_up</i>9.33%
                      </td>
                      <td><strong>$4536</strong></td>
                      <td class="green-text">Active</td>
                      <td class="center-align hide-on-small-only"><a class="dropdown-button page-opt-dropBtn btn-floating btn-flat waves-effect waves-set setWave" href="javascript:void(0)" data-activates="marketingCompaign" data-beloworigin="true" data-alignment="right" data-position="bottom" data-constrainwidth="false" data-delay="50" data-gutter="25"><i class="material-icons grey-text text-darken-2">more_vert</i></a></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="card">
              <div class="card-content">
                <div class="row no-mrpd">
                  <div class="col s8"><span class="card-title">Support Tickets</span></div>
                  <div class="col s4 right-align"><a class="dropdown-button page-opt-dropBtn btn-floating btn-flat waves-effect waves-set setWave" href="javascript:void(0)" data-activates="supportTicket" data-beloworigin="true" data-alignment="right" data-position="bottom" data-constrainwidth="false" data-delay="50" data-gutter="25"><i class="material-icons grey-text text-darken-2">menu</i></a></div>
                </div>
                <table class="responsive-table statistic-table">
                  <thead>
                    <tr>
                      <th class="user-th">User</th>
                      <th class="description-th">Description</th>
                      <th class="expand_more center-align">Due</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="grey-text"> <strong>Active Tickets</strong></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>
                        <div class="left"><a class="btn-floating waves-effect waves-set setWave pink accent-4 center-align" href="#">JW</a></div>
                        <div class="left pad-left-8">
                          <p>Joy Will</p>
                          <p class="blue-text"><small>Active</small></p>
                        </div>
                      </td>
                      <td><a class="black-text" href="#!"><strong>[#187] User widgets could not found.</strong></a>
                        <p class="grey-text text-darken-1">User widget usage is not described. How can I use user widget?</p>
                      </td>
                      <td class="center-align">
                        <p class="caption">3</p>
                        <p class="grey-text">Hours</p>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class="left"><a class="btn-floating waves-effect waves-set setWave amber accent-4 center-align" href="#">VC</a></div>
                        <div class="left pad-left-8">
                          <p>Valentina Champlin </p>
                          <p class="blue-text"><small>Active</small></p>
                        </div>
                      </td>
                      <td><a class="black-text" href="#!"><strong>[#185] How to deal with laravel multiauth?</strong></a>
                        <p class="grey-text text-darken-1">Need assistance to deal with Laravel Multiauth. It will be great if you could suggest module manager for it as well.</p>
                      </td>
                      <td class="center-align">
                        <p class="caption">16</p>
                        <p class="grey-text">Hours</p>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class="left"><a class="btn-floating waves-effect waves-set setWave blue accent-4 center-align" href="#">SM</a></div>
                        <div class="left pad-left-8">
                          <p>Stefany McDerious</p>
                          <p class="blue-text"><small>Active</small></p>
                        </div>
                      </td>
                      <td><a class="black-text" href="#!"><strong>[#184] Gulp Compilation getting stucked!</strong></a>
                        <p class="grey-text text-darken-1">Gulp command show an error: gulp does not exits.</p>
                      </td>
                      <td class="center-align">
                        <p class="caption">20</p>
                        <p class="grey-text">Hours</p>
                      </td>
                    </tr>
                    <tr>
                      <td class="green-text"> <strong>Resolved Tickets</strong></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>
                        <div class="left"><a class="btn-floating waves-effect waves-set setWave cyan accent-4 center-align" href="#">HG</a></div>
                        <div class="left pad-left-8">
                          <p>Haylie Gusikowski</p>
                          <p class="blue-text"><small>Resolved</small></p>
                        </div>
                      </td>
                      <td><a class="black-text" href="#!"><strong>[#179] Sass file megre is not working!</strong></a>
                        <p class="grey-text text-darken-1">Could not added newly created .sass file to main sass file.</p>
                      </td>
                      <td class="center-align"><a class="green-text" href="#!"><i class="material-icons">check</i></a></td>
                    </tr>
                    <tr>
                      <td>
                        <div class="left"><a class="btn-floating waves-effect waves-set setWave red accent-4 center-align" href="#">AS</a></div>
                        <div class="left pad-left-8">
                          <p>Anastasia Schmidt</p>
                          <p class="blue-text"><small>Resolved</small></p>
                        </div>
                      </td>
                      <td><a class="black-text" href="#!"><strong>[#176] Card spacing mismatch with card-dash</strong></a>
                        <p class="grey-text text-darken-1">Card spacing does not match in all cases. card-dash is varying in this case.</p>
                      </td>
                      <td class="center-align"><a class="green-text" href="#!"><i class="material-icons">check</i></a></td>
                    </tr>
                    <tr>
                      <td>
                        <div class="left"><a class="btn-floating waves-effect waves-set setWave orange accent-4 center-align" href="#">KM</a></div>
                        <div class="left pad-left-8">
                          <p>Kyle Moen</p>
                          <p class="blue-text"><small>Resolved</small></p>
                        </div>
                      </td>
                      <td><a class="black-text" href="#!"><strong>[#175] Accordian styles</strong></a>
                        <p class="grey-text text-darken-1">According(collapsible ) is not working with <code class="language-markup">.popout</code> class.</p>
                      </td>
                      <td class="center-align"><a class="green-text" href="#!"><i class="material-icons">check</i></a></td>
                    </tr>
                    <tr>
                      <td class="red-text"> <strong>Closed Tickets</strong></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>
                        <div class="left"><a class="btn-floating waves-effect waves-set setWave yellow accent-4 center-align" href="#">LB</a></div>
                        <div class="left pad-left-8">
                          <p>Lonny Beatty</p>
                          <p class="blue-text"><small>closed</small></p>
                        </div>
                      </td>
                      <td><a class="black-text" href="#!"><strong>[#177] Card Tab distubes in small area.</strong></a>
                        <p class="grey-text text-darken-1">Setting card with tab in small area is too difficult.</p>
                      </td>
                      <td class="center-align"><a class="red-text" href="#!"><i class="material-icons">close</i></a></td>
                    </tr>
                    <tr>
                      <td>
                        <div class="left"><a class="btn-floating waves-effect waves-set setWave green accent-4 center-align" href="#">JH</a></div>
                        <div class="left pad-left-8">
                          <p>Janae Hackett</p>
                          <p class="blue-text"><small>closed</small></p>
                        </div>
                      </td>
                      <td><a class="black-text" href="#!"><strong>[#160] Need more animations.</strong></a>
                        <p class="grey-text text-darken-1">Could we have some more animation in the theme?</p>
                      </td>
                      <td class="center-align"><a class="red-text" href="#!"><i class="material-icons">close</i></a></td>
                    </tr>
                    <tr>
                      <td>
                        <div class="left"><a class="btn-floating waves-effect waves-set setWave blue accent-4 center-align" href="#">PJ</a></div>
                        <div class="left pad-left-8">
                          <p>Patrick John</p>
                          <p class="blue-text"><small>closed</small></p>
                        </div>
                      </td>
                      <td><a class="black-text" href="#!"><strong>[#159] System architecture diagram</strong></a>
                        <p class="grey-text text-darken-1">Flow of company creation is not properly attached with other modules.</p>
                      </td>
                      <td class="center-align"><a class="red-text" href="#!"><i class="material-icons">close</i></a></td>
                    </tr>
                    <tr>
                      <td>
                        <div class="left"><a class="btn-floating waves-effect waves-set setWave pink accent-4 center-align" href="#">JK</a></div>
                        <div class="left pad-left-8">
                          <p>Joel Kodra</p>
                          <p class="blue-text"><small>closed</small></p>
                        </div>
                      </td>
                      <td><a class="black-text" href="#!"><strong>[#158] Change Request</strong></a>
                        <p class="grey-text text-darken-1">Support for the theme in Laravel 5.4 integration.</p>
                      </td>
                      <td class="center-align"><a class="red-text" href="#!"><i class="material-icons">close</i></a></td>
                    </tr>
                    <tr>
                      <td>
                        <div class="left"><a class="btn-floating waves-effect waves-set setWave cyan accent-4 center-align" href="#">AS</a></div>
                        <div class="left pad-left-8">
                          <p>Ashly Simpsons</p>
                          <p class="blue-text"><small>closed</small></p>
                        </div>
                      </td>
                      <td><a class="black-text" href="#!"><strong>[#157] Change Request : VueJS</strong></a>
                        <p class="grey-text text-darken-1">Expecting a launch for VueJS version integrated with Forge.</p>
                      </td>
                      <td class="center-align"><a class="red-text" href="#!"><i class="material-icons">close</i></a></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="col s12 m12 l4">
            <div class="card card-user-widget01">
              <div class="card-image bg-only">
                <div class="overlay deep-purple-overlay"></div><img class="bg-placeholder" src="{{ URL::asset('public/theme/images/placeholder/1000x700g.jpg') }}" alt="placeholder"><a class="card-userpic"><img src="{{ URL::asset('public/theme/images/placeholder/300x300t.jpg') }}" alt="placeholder"></a><a class="btn-floating halfway-fab waves-effect waves-light secondary-bg"><i class="material-icons">add</i></a>
              </div>
              <div class="card-content">
                <h5 class="center grey-text text-darken-1">Slade Wilson</h5>
                <p class="center">Web Designer </p>
                <div class="collection widget-inline-static"><a class="collection-item inline-static-item col s4" href="#!"><i class="material-icons secondary-text">remove_red_eye</i></a><a class="collection-item inline-static-item col s4" href="#!"><i class="material-icons secondary-text">person_add</i></a><a class="collection-item inline-static-item col s4" href="#!"><i class="material-icons secondary-text">message</i></a></div>
              </div>
            </div>
            <div class="card-panel snow-grade">
              <div class="row">
                <div class="col s8 no-mrpd">
                  <h6 class="no-mrpd white-text">Washington</h6>
                  <p class="no-mrpd white-text">Distroct of Columbia</p>
                </div>
                <div class="col s4 no-mrpd">
                  <svg class="weather2 snow-cloud fill-white" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewbox="0 0 512 512">
                    <path d="M512,176c0-61.8-50.2-112-112-112c-5.3,0-10.6,0.4-15.8,1.1C354.3,24.4,307.2,0,256,0s-98.3,24.4-128.2,65.1			c-5.2-0.8-10.5-1.1-15.8-1.1C50.2,64,0,114.2,0,176s50.2,112,112,112c13.7,0,27.1-2.5,39.7-7.3c29,25.2,65.8,39.3,104.3,39.3			c38.5,0,75.3-14.1,104.3-39.3c12.6,4.8,26,7.3,39.7,7.3C461.8,288,512,237.8,512,176z M354.1,241.3C330.6,269.6,295.6,288,256,288			c-39.6,0-74.6-18.4-98.1-46.7c-13,9.2-28.8,14.7-45.9,14.7c-44.2,0-80-35.8-80-80s35.8-80,80-80c10.8,0,21.1,2.2,30.4,6.1			C163.7,60.7,206.3,32,256,32s92.3,28.7,113.5,70.1c9.4-3.9,19.7-6.1,30.5-6.1c44.2,0,80,35.8,80,80s-35.8,80-80,80			C382.9,256,367.1,250.5,354.1,241.3z"></path>
                    <path class="snowflake-one" d="M131.8,349.9c-1.5-5.6-7.3-8.9-12.9-7.4l-11.9,3.2c-1.1-1.5-2.2-3-3.6-4.4c-1.4-1.4-2.9-2.6-4.5-3.6l3.2-11.9			c1.5-5.6-1.8-11.4-7.4-12.9c-5.6-1.5-11.4,1.8-12.9,7.4l-3.2,12.1c-3.8,0.3-7.5,1.2-10.9,2.9l-8.8-8.8c-4.1-4.1-10.8-4.1-14.8,0			c-4.1,4.1-4.1,10.8,0,14.9l8.8,8.8c-1.6,3.5-2.6,7.2-2.9,11l-12,3.2c-5.6,1.5-9,7.2-7.5,12.9c1.5,5.6,7.3,8.9,12.9,7.4l11.9-3.2			c1.1,1.6,2.2,3.1,3.7,4.5c1.4,1.4,2.9,2.6,4.4,3.6l-3.2,11.9c-1.5,5.6,1.8,11.4,7.4,12.9c5.6,1.5,11.3-1.8,12.8-7.4l3.2-12			c3.8-0.3,7.5-1.3,11-2.9l8.8,8.8c4.1,4.1,10.7,4,14.8,0c4.1-4.1,4.1-10.7,0-14.8l-8.8-8.8c1.7-3.5,2.7-7.2,2.9-11l12.1-3.2			C130,361.3,133.3,355.6,131.8,349.9z M88.6,371c-4.1,4.1-10.8,4.1-14.9,0c-4.1-4.1-4.1-10.8,0-14.8c4.1-4.1,10.8-4.1,14.9,0			S92.6,366.9,88.6,371z"></path>
                    <path class="snowflake-two" d="M304.8,437.6l-12.6-7.2c0.4-2.2,0.7-4.4,0.7-6.7c0-2.3-0.3-4.5-0.7-6.7l12.6-7.2c5.9-3.4,7.9-11,4.5-16.8			c-3.4-5.9-10.9-7.9-16.8-4.5l-12.7,7.3c-3.4-2.9-7.2-5.2-11.5-6.7v-14.6c0-6.8-5.5-12.3-12.3-12.3s-12.3,5.5-12.3,12.3V389			c-4.3,1.5-8.1,3.8-11.5,6.7l-12.7-7.3c-5.9-3.4-13.5-1.4-16.9,4.5c-3.4,5.9-1.4,13.4,4.5,16.8l12.5,7.2c-0.4,2.2-0.7,4.4-0.7,6.7			c0,2.3,0.3,4.5,0.7,6.7l-12.5,7.2c-5.9,3.4-7.9,11-4.5,16.9s10.9,7.9,16.8,4.5l12.7-7.3c3.4,2.9,7.2,5.1,11.5,6.7V473			c0,6.8,5.5,12.3,12.3,12.3s12.3-5.5,12.3-12.3v-14.6c4.3-1.5,8.2-3.8,11.5-6.7l12.7,7.3c5.9,3.4,13.4,1.4,16.8-4.5			C312.8,448.6,310.7,441.1,304.8,437.6z M256,436c-6.8,0-12.3-5.5-12.3-12.3c0-6.8,5.5-12.3,12.3-12.3s12.3,5.5,12.3,12.3			C268.3,430.5,262.8,436,256,436z"></path>
                    <path class="snowflake-three" d="M474.2,396.2l-12.1-3.2c-0.3-3.8-1.2-7.5-2.9-11l8.8-8.8c4.1-4.1,4.1-10.8,0-14.9c-4.1-4.1-10.7-4.1-14.8,0			l-8.8,8.8c-3.5-1.6-7.1-2.6-11-2.9l-3.2-12.1c-1.5-5.6-7.2-8.9-12.9-7.4c-5.6,1.5-8.9,7.3-7.4,12.9l3.2,11.9			c-1.6,1.1-3.1,2.3-4.5,3.7c-1.4,1.4-2.5,2.9-3.6,4.5l-11.9-3.2c-5.6-1.5-11.4,1.9-12.9,7.4c-1.5,5.6,1.9,11.4,7.4,12.9l12,3.2			c0.3,3.8,1.3,7.5,3,11l-8.8,8.8c-4.1,4.1-4.1,10.7,0,14.8c4.1,4.1,10.7,4.1,14.8,0l8.8-8.8c3.5,1.7,7.2,2.7,11,3l3.2,12			c1.5,5.6,7.2,8.9,12.9,7.4c5.6-1.5,9-7.2,7.5-12.9l-3.2-11.9c1.5-1.1,3-2.2,4.5-3.6c1.4-1.4,2.5-2.9,3.6-4.5l11.9,3.2			c5.6,1.5,11.4-1.9,12.9-7.4C483.1,403.5,479.8,397.8,474.2,396.2z M438.3,402.9c-4.1,4.1-10.8,4.1-14.9,0c-4.1-4.1-4.1-10.7,0-14.9			c4.1-4.1,10.8-4.1,14.9,0C442.4,392.2,442.4,398.9,438.3,402.9z"></path>
                  </svg>
                </div>
              </div>
              <div class="row">
                <div class="col s8 no-mrpd">
                  <h2 class="left no-mrpd white-text">-06<i class="wi wi-celsius"></i>
                  </h2>
                  <p class="left sticky-bottom-lr white-text"><i class="wi wi-direction-right"></i><i class="wi wi-snowflake-cold"></i>50%
                  </p>
                </div>
                <div class="col s4">
                  <p class="left white-text">20:19</p>
                </div>
              </div>
            </div>
            <div class="user-widget">
              <div class="card blue-grey darken-1 user-widget-type4">
                <div class="card-image"><img class="responsive-img" src="{{ URL::asset('public/theme/images/placeholder/1280x853g.jpg') }}" alt="placeholder">
                  <div class="card-title center">Selena Cruiz</div><a class="btn-floating np-bg btn-flat waves-effect waves-light activator" href="javascript:void(0)"><i class="material-icons grey-text">more_vert</i></a>
                </div>
                <div class="card-content">
                  <div class="designation center grey-text text-darken-3">Designer</div>
                  <div class="socialization center"><a class="btn-floating  grey darken-3 btn-flat waves-effect waves-light" href="#!"><i class="mdi mdi-facebook"></i></a><a class="btn-floating  grey darken-3 btn-flat waves-effect waves-light" href="#!"><i class="mdi mdi-twitter"></i></a><a class="btn-floating  grey darken-3 btn-flat waves-effect waves-light" href="#!"><i class="mdi mdi-github-circle"></i></a><a class="btn-floating  grey darken-3 btn-flat waves-effect waves-light" href="#!"><i class="mdi mdi-google-plus"></i></a></div>
                </div>
                <div class="card-reveal"><span class="card-title grey-text text-darken-4">Selena Cruiz<i class="material-icons right">close</i></span>
                  <p>Here is some more information about this product that is only revealed once clicked on.</p>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-content"><span class="card-title">Application Requests</span>
                <table>
                  <thead>
                    <tr>
                      <th class="application-th">Applicatioin</th>
                      <th class="time-th">Time</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        <div class="left"><a class="btn-floating waves-effect waves-set setWave brown accent-4 center-align" href="#">GK</a></div>
                        <div class="left pad-left-8">
                          <p>Greenholt-Kiehn</p><a class="green-text" href="#!">New<i class="material-icons left">add</i></a>
                        </div>
                      </td>
                      <td class="center-align">
                        <p class="grey-text">08:00AM</p>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class="left"><a class="btn-floating waves-effect waves-set setWave red accent-3 center-align" href="#">WO</a></div>
                        <div class="left pad-left-8">
                          <p>Ward-O'Keefe</p><a class="blue-text" href="#!">Renew<i class="material-icons left">autorenew</i></a>
                        </div>
                      </td>
                      <td class="center-align">
                        <p class="grey-text">09:00AM</p>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class="left"><a class="btn-floating waves-effect waves-set setWave  light-blue accent-3 center-align" href="#">RG</a></div>
                        <div class="left pad-left-8">
                          <p>Reichert Group</p><a class="amber-text" href="#!">Support<i class="material-icons left">language</i></a>
                        </div>
                      </td>
                      <td class="center-align">
                        <p class="grey-text">11:00AM</p>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class="left"><a class="btn-floating waves-effect waves-set setWave yellow center-align" href="#">SB</a></div>
                        <div class="left pad-left-8">
                          <p>Simonis-Breiten</p><a class="blue-text" href="#!">Renew<i class="material-icons left">autorenew</i></a>
                        </div>
                      </td>
                      <td class="center-align">
                        <p class="grey-text">01:00AM</p>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div class="left"><a class="btn-floating waves-effect waves-set setWave cyan accent-3 center-align" href="#">C</a></div>
                        <div class="left pad-left-8">
                          <p>Collier Ltd</p><a class="green-text" href="#!">New<i class="material-icons left">add</i></a>
                        </div>
                      </td>
                      <td class="center-align">
                        <p class="grey-text">02:00AM</p>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="card">
              <div class="card-image">
                <div class="carousel carousel-slider"><a class="carousel-item" href="#one!"><img src="http://forge.codigoforge.com/lorempixel/720x400/food/1" alt="placeholder"></a><a class="carousel-item" href="#two!"><img src="http://forge.codigoforge.com/lorempixel/720x400/food/2" alt="placeholder"></a><a class="carousel-item" href="#three!"><img src="http://forge.codigoforge.com/lorempixel/720x400/food/3" alt="placeholder"></a><a class="carousel-item" href="#four!"><img src="http://forge.codigoforge.com/lorempixel/720x400/food/4" alt="placeholder"></a></div>
              </div>
              <div class="card-action">
                <ul class="collection horz right-align">
                  <li class="collection-item"><a class="primary-text" href="#"><i class="material-icons">favorite</i></a></li>
                  <li class="collection-item"><a class="primary-text" href="#"><i class="material-icons">share</i></a></li>
                  <li class="collection-item"><a class="primary-text" href="#"><i class="material-icons">bookmark</i></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col s12 m6">
            <div class="card card-dash">
              <div class="card-header primary-bg z-depth-2"><i class="material-icons left">style</i><span class="caption">Products Type</span>
              </div>
              <div class="card-content">
                <p class="category">Sales: 128/day (Avg)</p>
                <canvas id="productTypeSales" height="140"></canvas>
              </div>
              <div class="card-footer">
                <div class="stats"><i class="material-icons red-text">favorite</i><a href="#">HTML Template most favorite.</a>
                </div>
              </div>
            </div>
          </div>
          <div class="col s12 m6">
            <div class="card card-dash">
              <div class="card-header secondary-bg light z-depth-2"><i class="material-icons left">star</i><span class="caption">Traffic Audiance</span>
              </div>
              <div class="card-content">
                <p class="category">Visits: 20K/day</p>
                <canvas id="trafficAudiance" height="140"></canvas>
              </div>
              <div class="card-footer">
                <div class="stats"><i class="material-icons red-text">import_export</i><a href="#">Export Statistic</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col s12 m12 l6">
            <div class="card">
              <div class="card-content"><span class="card-title">Projects</span>
                <table class="responsive-table">
                  <thead class="primary-text">
                    <tr>
                      <th data-field="id">ID</th>
                      <th data-field="name">Name</th>
                      <th data-field="completed">Completed</th>
                      <th data-field="data">Date</th>
                      <th data-field="action">Action</th>
                    </tr>
                  </thead>
                  <tbody class="black-text">
                    <tr>
                      <td>1</td>
                      <td>Forge</td>
                      <td><span class="taskred75"></span></td>
                      <td>April 14,2017</td>
                      <td style="width:25%;">
                        <div class="action-btns"><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons primary-text">edit</i></a><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons secondary-text">close</i></a></div>
                      </td>
                    </tr>
                    <tr>
                      <td>2</td>
                      <td>Forge Laravel</td>
                      <td><span class="taskyellow50"></span></td>
                      <td>May 17,2017</td>
                      <td style="width:25%;">
                        <div class="action-btns"><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons primary-text">edit</i></a><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons secondary-text">close</i></a></div>
                      </td>
                    </tr>
                    <tr>
                      <td>3</td>
                      <td>Forge Vue</td>
                      <td><span class="taskblue25"></span></td>
                      <td>August 8,2017</td>
                      <td style="width:25%;">
                        <div class="action-btns"><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons primary-text">edit</i></a><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons secondary-text">close</i></a></div>
                      </td>
                    </tr>
                    <tr>
                      <td>4</td>
                      <td>CodigoForge</td>
                      <td><span class="taskgreen"></span></td>
                      <td>Novemver 26,2017</td>
                      <td style="width:25%;">
                        <div class="action-btns"><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons primary-text">edit</i></a><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons secondary-text">close</i></a></div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="col s12 m12 l6">
            <div class="card">
              <div class="card-content"><span class="card-title">Projects Execution</span>
                <table>
                  <thead class="primary-text">
                    <tr>
                      <th data-field="id">ID</th>
                      <th data-field="name">Name</th>
                      <th class="center-align" data-field="Status">Status</th>
                      <th class="center-align" data-field="progress">Progress</th>
                    </tr>
                  </thead>
                  <tbody class="black-text">
                    <tr class="height-60">
                      <td>1</td>
                      <td>Prepare flow charts</td>
                      <td class="center-align"><span class="success-bg project-status">Completed</span></td>
                      <td class="center-align"><span class="progress1"></span></td>
                    </tr>
                    <tr class="height-60">
                      <td>2</td>
                      <td>Define database schema</td>
                      <td class="center-align"><span class="info-bg project-status">Developing</span></td>
                      <td class="center-align"><span class="progress2"></span></td>
                    </tr>
                    <tr class="height-60">
                      <td>3</td>
                      <td>Define module sequence</td>
                      <td class="center-align"><span class="error-bg project-status">Cancelled</span></td>
                      <td class="center-align"><span class="progress3"></span></td>
                    </tr>
                    <tr class="height-60">
                      <td>4</td>
                      <td>Choose Technologies</td>
                      <td class="center-align"><span class="warning-bg project-status">In Progress</span></td>
                      <td class="center-align"><span class="progress4"></span></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col s12 m12 l4">
            <div class="card card-dash">
              <div class="card-header primary-bg dark">
                <div class="card-tabs">
                  <ul class="tabs tabs-fixed-width tabs-transparent">
                    <li class="tab"><a href="#dash-bugs">Bugs</a></li>
                    <li class="tab"><a href="#dash-web">Website</a></li>
                    <li class="tab"><a href="#dash-server">Server</a></li>
                  </ul>
                </div>
              </div>
              <div class="card-content">
                <div id="dash-bugs">
                  <table>
                    <tbody>
                      <tr>
                        <td style="width:10%;">
                          <input class="filled-in" type="checkbox" id="bug1" checked>
                          <label class="block-ele" for="bug1"></label>
                        </td>
                        <td class="black-text">System execute takes much time. Optimization required.</td>
                        <td style="width:25%;">
                          <div class="action-btns"><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons primary-text">edit</i></a><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons secondary-text">close</i></a></div>
                        </td>
                      </tr>
                      <tr>
                        <td style="width:25px;">
                          <input class="filled-in" type="checkbox" id="bug2">
                          <label class="block-ele" for="bug2"></label>
                        </td>
                        <td class="black-text">Email goes in spam.</td>
                        <td style="width:25%;">
                          <div class="action-btns"><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons primary-text">edit</i></a><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons secondary-text">close</i></a></div>
                        </td>
                      </tr>
                      <tr>
                        <td style="width:25px;">
                          <input class="filled-in" type="checkbox" id="bug3">
                          <label class="block-ele" for="bug3"></label>
                        </td>
                        <td class="black-text">Site color and fonts are not matched with PSD.</td>
                        <td style="width:25%;">
                          <div class="action-btns"><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons primary-text">edit</i></a><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons secondary-text">close</i></a></div>
                        </td>
                      </tr>
                      <tr>
                        <td style="width:25px;">
                          <input class="filled-in" type="checkbox" id="bug4">
                          <label class="block-ele" for="bug4"></label>
                        </td>
                        <td class="black-text">Page should not take much time to load. Assets & code optimization is required.</td>
                        <td style="width:25%;">
                          <div class="action-btns"><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons primary-text">edit</i></a><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons secondary-text">close</i></a></div>
                        </td>
                      </tr>
                      <tr>
                        <td style="width:25px;">
                          <input class="filled-in" type="checkbox" id="bug5">
                          <label class="block-ele" for="bug5"></label>
                        </td>
                        <td class="black-text">Menu is not working properly in Mobile device.</td>
                        <td style="width:25%;">
                          <div class="action-btns"><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons primary-text">edit</i></a><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons secondary-text">close</i></a></div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div id="dash-web">
                  <table>
                    <tbody>
                      <tr>
                        <td style="width:10%;">
                          <input class="filled-in" type="checkbox" id="web1" checked>
                          <label class="block-ele" for="web1"></label>
                        </td>
                        <td class="black-text">Get the list of material source.</td>
                        <td style="width:25%;">
                          <div class="action-btns"><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons primary-text">edit</i></a><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons secondary-text">close</i></a></div>
                        </td>
                      </tr>
                      <tr>
                        <td style="width:25px;">
                          <input class="filled-in" type="checkbox" id="web2">
                          <label class="block-ele" for="web2"></label>
                        </td>
                        <td class="black-text">Design achitecture of website</td>
                        <td style="width:25%;">
                          <div class="action-btns"><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons primary-text">edit</i></a><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons secondary-text">close</i></a></div>
                        </td>
                      </tr>
                      <tr>
                        <td style="width:25px;">
                          <input class="filled-in" type="checkbox" id="web3">
                          <label class="block-ele" for="web3"></label>
                        </td>
                        <td class="black-text">List down all feature and choose bundle manager which will handle assets.</td>
                        <td style="width:25%;">
                          <div class="action-btns"><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons primary-text">edit</i></a><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons secondary-text">close</i></a></div>
                        </td>
                      </tr>
                      <tr>
                        <td style="width:25px;">
                          <input class="filled-in" type="checkbox" id="web4">
                          <label class="block-ele" for="web4"></label>
                        </td>
                        <td class="black-text">Optimize for quick loading contents.</td>
                        <td style="width:25%;">
                          <div class="action-btns"><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons primary-text">edit</i></a><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons secondary-text">close</i></a></div>
                        </td>
                      </tr>
                      <tr>
                        <td style="width:25px;">
                          <input class="filled-in" type="checkbox" id="web5">
                          <label class="block-ele" for="web5"></label>
                        </td>
                        <td class="black-text">Define base color palette and fonts readability.</td>
                        <td style="width:25%;">
                          <div class="action-btns"><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons primary-text">edit</i></a><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons secondary-text">close</i></a></div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div id="dash-server">
                  <table>
                    <tbody>
                      <tr>
                        <td style="width:10%;">
                          <input class="filled-in" type="checkbox" id="server1" checked>
                          <label class="block-ele" for="server1"></label>
                        </td>
                        <td class="black-text">Inquire about server and its facilities with cost.</td>
                        <td style="width:25%;">
                          <div class="action-btns"><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons primary-text">edit</i></a><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons secondary-text">close</i></a></div>
                        </td>
                      </tr>
                      <tr>
                        <td style="width:25px;">
                          <input class="filled-in" type="checkbox" id="server2">
                          <label class="block-ele" for="server2"></label>
                        </td>
                        <td class="black-text">Choose server and operating system.</td>
                        <td style="width:25%;">
                          <div class="action-btns"><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons primary-text">edit</i></a><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons secondary-text">close</i></a></div>
                        </td>
                      </tr>
                      <tr>
                        <td style="width:25px;">
                          <input class="filled-in" type="checkbox" id="server3">
                          <label class="block-ele" for="server3"></label>
                        </td>
                        <td class="black-text">Install essenstial software and extenstions.</td>
                        <td style="width:25%;">
                          <div class="action-btns"><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons primary-text">edit</i></a><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons secondary-text">close</i></a></div>
                        </td>
                      </tr>
                      <tr>
                        <td style="width:25px;">
                          <input class="filled-in" type="checkbox" id="server4">
                          <label class="block-ele" for="server4"></label>
                        </td>
                        <td class="black-text">Google Scalling tool implementation.</td>
                        <td style="width:25%;">
                          <div class="action-btns"><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons primary-text">edit</i></a><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons secondary-text">close</i></a></div>
                        </td>
                      </tr>
                      <tr>
                        <td style="width:25px;">
                          <input class="filled-in" type="checkbox" id="server5">
                          <label class="block-ele" for="server5"></label>
                        </td>
                        <td class="black-text">Check AWS Route53 host zone and S3 permission policy.</td>
                        <td style="width:25%;">
                          <div class="action-btns"><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons primary-text">edit</i></a><a class="btn-floating waves-effect waves-light btn-flat" href="javascript:void(0)"><i class="material-icons secondary-text">close</i></a></div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="col s12 m12 l4 user-widget">
            <div class="card blue-grey darken-1 user-widget-type3">
              <div class="card-image"><img class="responsive-img" src="{{ URL::asset('public/theme/images/placeholder/1024x600t.jpg') }}" alt="placeholder">
                <div class="card-title white-text">Mack Carroll</div>
                <div class="internal-action-top">
                  <div class="user-action">
                    <div class="actions-list no-fixed-action"><a class="btn-floating pink lighten-2"><i class="material-icons">favorite</i></a><a class="btn-floating green"><i class="material-icons">chat</i></a><a class="btn-floating blue lighten-2"><i class="material-icons">rss_feed</i></a></div>
                  </div>
                </div>
              </div>
              <div class="card-content">
                <div class="row user-statistics">
                  <div class="col s12 with-top-text">
                    <button class="s4 waves-effect waves-light col btn-flat"><span class="statistic-title">Friends</span><span class="counter">137</span></button>
                    <button class="s4 waves-effect waves-light col btn-flat"><span class="statistic-title">Followers</span><span class="counter">137</span></button>
                    <button class="s4 waves-effect waves-light col btn-flat"><span class="statistic-title">Following</span><span class="counter">137</span></button>
                  </div>
                </div>
              </div>
              <div class="card-action"><a class="waves-effect waves-light btn basic-btn" href="#"><i class="material-icons left">person_add</i>Send Request</a><a class="activator waves-effect waves-light btn-floating no-bg btn-flat right" href="javascript:void(0)"> <i class="material-icons grey-text text-darken-4">more_vert</i></a></div>
              <div class="card-reveal"><span class="card-title grey-text text-darken-4">Card Title<i class="material-icons right">close</i></span>
                <p>Here is some more information about this product that is only revealed once clicked on.</p>
              </div>
            </div>
          </div>
          <div class="col s12 m12 l4 user-widget">
            <div class="card blue-grey darken-1 user-widget-type3">
              <div class="card-image"><img class="responsive-img" src="{{ URL::asset('public/theme/images/placeholder/1024x600t.jpg') }}" alt="placeholder">
                <div class="card-title">Kennedy Wisozk</div>
              </div>
              <div class="card-content">
                <p>I am a very simple card. I am good at containing small bits of information.</p>
              </div>
              <div class="card-action"><a class="waves-effect waves-light btn basic-btn" href="#"><i class="material-icons left">person_add</i>Send Request</a><a class="activator waves-effect waves-light btn-floating no-bg btn-flat right" href="javascript:void(0)"> <i class="material-icons grey-text text-darken-4">more_vert</i></a></div>
              <div class="card-reveal"><span class="card-title grey-text text-darken-4">Card Title<i class="material-icons right">close</i></span>
                <p>Here is some more information about this product that is only revealed once clicked on.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      @include('includes/footer') 