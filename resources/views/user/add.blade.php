@include('includes/header')
    <main class="animated">
      <ul class="dropdown-content action-ex-opts" id="generalDropDown">
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">account_box</i><span>Account</span></a></li>
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">local_activity</i><span>Recent Activities</span></a></li>
        <li><a class="waves-effect waves-set" href="#"><i class="mdi mdi-wheelchair-accessibility"></i><span>Accessibility</span></a></li>
        <li class="divider"></li>
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">settings</i><span>All Settings</span></a></li>
      </ul>
      <ul class="dropdown-content action-ex-opts" id="marketingCompaign">
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">account_box</i><span>Account</span></a></li>
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">local_activity</i><span>Recent Activities</span></a></li>
        <li><a class="waves-effect waves-set" href="#"><i class="mdi mdi-wheelchair-accessibility"></i><span>Accessibility</span></a></li>
        <li class="divider"></li>
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">settings</i><span>All Settings</span></a></li>
      </ul>
      <ul class="dropdown-content action-ex-opts" id="marketingCompaignInternal">
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">account_box</i><span>Account</span></a></li>
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">local_activity</i><span>Recent Activities</span></a></li>
        <li><a class="waves-effect waves-set" href="#"><i class="mdi mdi-wheelchair-accessibility"></i><span>Accessibility</span></a></li>
        <li class="divider"></li>
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">settings</i><span>All Settings</span></a></li>
      </ul>
      <ul class="dropdown-content action-ex-opts" id="supportTicket">
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">account_box</i><span>Account</span></a></li>
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">local_activity</i><span>Recent Activities</span></a></li>
        <li><a class="waves-effect waves-set" href="#"><i class="mdi mdi-wheelchair-accessibility"></i><span>Accessibility</span></a></li>
        <li class="divider"></li>
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">settings</i><span>All Settings</span></a></li>
      </ul>
      <ul class="dropdown-content action-ex-opts" id="supportTicketInternal">
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">account_box</i><span>Account</span></a></li>
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">local_activity</i><span>Recent Activities</span></a></li>
        <li><a class="waves-effect waves-set" href="#"><i class="mdi mdi-wheelchair-accessibility"></i><span>Accessibility</span></a></li>
        <li class="divider"></li>
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">settings</i><span>All Settings</span></a></li>
      </ul>
      <div class="main-header">
        <div class="sec-page">
          <div class="page-title">
            <h2>Users</h2>
          </div>
        </div>
      </div>
      <div class="main-container">
        <div class="row">
          <div class="col s12 m12">
            <div class="card card-dash">
            <form method="post" id="addUserForm" action="{{ route('userAddProcess') }}">
            {{ csrf_field() }}
              <div class="card-header secondary-bg light z-depth-2"><i class="material-icons left">star</i><span class="caption">Add New User</span>
              </div>
              <div class="card-content">
                    <div class="row">
                            <div class="input-field col s6">
                                <input type="text" id="name" name="name" placeholder="Name" class="validate">
                                <label for="name">Name</label>
                            </div>
                            <div class="input-field col s6">
                                <input type="email" id="email" name="email" placeholder="Email" class="validate">
                                <label for="email">Email</label>
                            </div>
                            <div class="input-field col s6">
                                <input type="password" id="password" name="password" placeholder="Paasword" class="validate">
                                <label for="password">Password</label>
                            </div>
                            <div class="input-field col s6">
                                <select class="mat_select" id="role" name="role">
                                    <option value="" disabled selected>Select Role</option>
                                    <option value="administrator">Administrator</option>
                                    <option value="agent">Agent</option>
                                    <option value="customer">Customer</option>
                                </select>
                                <label for="role">Role</label>
                            </div>
                        </div>
                    </div>
              <div class="card-footer right">
                <button class="btn waves-effect waves-light" type="submit" name="action">Submit
                    <i class="material-icons right">send</i>
                </button>
              </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      @include('includes/footer') 