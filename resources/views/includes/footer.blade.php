<!-- FOOTER-->
<footer class="page-footer no-mrpd grey lighten-4">
        <div class="footer-copyright">
          <div class="container primary-text">© <?php echo date('Y'); ?> Adwords, All rights reserved.
            <div class="right"><span class="left">Handcrafted with </span><a class="left" href="#"><i class="material-icons secondary-text tiny left" style="line-height:20px;margin:0px 8px;">favorite</i></a></div>
          </div>
        </div>
      </footer>
    </main>
    <!-- ============================-->
    <!-- SCRIPTS-->
    <!-- ============================-->
    <script type="text/javascript" src="{{ URL::asset('public/theme/js/all.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('public/theme/plugins/charts/chartjs/dist/Chart.bundle.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('public/theme/plugins/sparkline/jquery.sparkline.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('public/theme/plugins/charts/google-chart/loader.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('public/theme/plugins/charts/google-chart/jsapi.js') }}"></script>
    <script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAoqM2ajGLymYCyt14bhxOEfcjHDShZDm8" type="text/javascript"></script>
    <script src="{{ URL::asset('public/theme/js/dashboard.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    <script type="text/javascript" src="{{ URL::asset('public/theme/js/custom.js') }}"></script>
  </body>
</html>