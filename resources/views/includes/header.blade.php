<!DOCTYPE html>
<html class="loading" lang="en">
  <!-- Item Name: Forge - Material Design Admin Template
  Version: 1.0
  Author: CodigoForge
  Author URL: https://themeforest.net/user/codigoforge
  WebSite : http://www.codigoforge.com
  -->
  <head>
    <link rel="icon" href="{{ URL::asset('public/theme/images/favicon/favicon-32x32.png') }}" sizes="32x32">
    <link rel="apple-touch-icon-precomposed" href="{{ URL::asset('public/theme/images/favicon/favicon-32x32.png') }}">
    <!-- ============================-->
    <!-- META DATA-->
    <!-- ============================-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="msapplication-TileColor" content="#FFFFFF">
    <meta name="msapplication-TileImage" content="{{ URL::asset('public/theme/images/favicon/favicon-32x32.png') }}">
    <meta name="theme-color" content="#2a56c6">
    <!-- ============================-->
    <!-- TITLE-->
    <!-- ============================-->
    <title>Adwords | Dashboard</title>
    <!-- ============================-->
    <!-- FONTS-->
    <!-- ============================-->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Inconsolata" type="text/css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/icon?family=Material+Icons">
    <!-- ============================-->
    <!-- CSS-->
    <!-- ============================-->
    <link rel="stylesheet" href="{{ URL::asset('public/theme/css/main.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('public/theme/css/dynamic.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('public/theme/css/markup.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('public/theme/images/plugins/scrollbar/perfect-scrollbar.min.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ URL::asset('public/theme/images/css/plugins/c3/c3.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('public/theme/css/custom.css') }}">
    </head>
  <body>
    <!-- ============================-->
    <!-- CONTENT AREA-->
    <!-- ============================-->
    <header>
      <!-- ============================-->
      <!-- TOP NAV-->
      <!-- ============================-->
      <div class="navbar-fixed full-top-nav">
        <div id="current-menu" data-menu="default">
          <nav><a class="morph menu small mob-menu button-collapse top-nav waves-effect waves-light circle hide-on-large-only" href="javascript:void(0)" id="sidebar-default-collapse" data-activates="nav-default"><span><span class="s1"></span><span class="s2"></span><span class="s3"></span></span></a>
            <div class="nav-wrapper">
              <!-- LOGO Set--><a class="animated brand-logo hide-on-large-only nav-logo" href="javascript:void(0)"><span class="left">ADWORDS</span></a><a class="animated brand-logo hide-on-med-and-down defaultMenu-logo" href="javascript:void(0)"><span class="left">ADWORDS</span></a>
              <!-- Left menu options at top-nav-->
              <ul class="left topnav-Menu-ls hide-on-med-and-down">
                <li><a class="morph small iconizedToggle waves-effect waves-light" href="javascript:void(0)"><span><span class="s1"></span><span class="s2"></span><span class="s3"></span></span></a>
                </li>
              </ul>
              <!-- Right Menu-->
              <ul class="right">
                <li class="hide-on-med-and-down"><a class="waves-effect waves-set app-search-btn" href="#"><i class="material-icons">search</i></a></li>
                <!-- MESSAGE SECTION-->
                <li class="hide-on-med-and-down"><a class="dropdown-button waves-effect waves-set" href="#" data-beloworigin="true" data-activates="top-nav-msgsweb"><i class="material-icons">email</i><span class="badge-count">5</span></a></li>
                <!-- ADMIN SETTINGS SECTION-->
                <li><a class="dropdown-button waves-effect waves-set" href="#" data-beloworigin="true" data-activates="top-nav-userProfile"><img class="circle admin-profile-img-small" src="{{ URL::asset('public/theme/images/placeholder/50x50g.jpg') }}" alt=""></a></li>
                <li class="hide-on-large-only"><a class="waves-effect waves-set toggle-topnav-hidden-menu" href="javascript:void(0);"><i class="material-icons">more_vert</i></a></li>
              </ul>
              <!-- Mobile Screen Nav Options-->
              <ul class="right hide-on-large-only topnav-hidden-menu hide">
                <li><a class="waves-effect waves-set app-search-btn" href="#"><i class="material-icons">search</i></a></li>
                <li class="right"><a class="waves-effect waves-set toggle-topnav-hidden-menu" href="#"><i class="material-icons">more_vert</i></a></li>
                <li class="right"><a class="dropdown-button waves-effect waves-set" href="#" data-beloworigin="true" data-activates="top-nav-msgs"><i class="material-icons">email</i><span class="badge-count">5</span></a></li>
              </ul>
              <!-- DROP-DOWN-->
              <div class="drop-down-bucket">
                <ul class="collection dropdown-content" id="top-nav-msgsweb">
                  <li class="collection-item msg-header">
                    <ul>
                      <li class="left"><span>Messages</span><a class="right" href="#"><i class="material-icons small">create</i></a></li>
                    </ul>
                  </li>
                  <li class="collection-item msg-content">
                    <ul class="collection" id="psTopNavMmsgsWeb">
                      <li class="collection-item msg-items">
                        <!-- REDIRECTION TO User-Profile--><a class="chat-user" href="#"> <img class="circle" src="{{ URL::asset('public/theme/images/placeholder/300x300t.jpg') }}" alt=""><span class="chat-status with-badge online">10</span></a>
                        <!-- Redirection to msg/chat--><a class="chat-content" href="#"><span class="title">Jenny Jordan</span>
                          <div class="notify-desc">
                             Hey, How are you doing?</div><span class="chat-time">30 Minutes ago.</span></a>
                      </li>
                    </ul>
                  </li>
                  <li class="collection-item msg-footer pos-relative"><a class="center tooltipped" href="#" data-position="top" data-tooltip="See all messages" style="padding:7px 16px;"><i class="material-icons small clear-style">more_horiz</i></a></li>
                </ul>
                <ul class="collection dropdown-content" id="top-nav-msgs">
                  <li class="collection-item msg-header">
                    <ul>
                      <li class="left"><span>Messages</span><a class="right" href="#"><i class="material-icons small">create</i></a></li>
                    </ul>
                  </li>
                  <li class="collection-item msg-content">
                    <ul class="collection" id="psTopNavMmsgs">
                      <li class="collection-item msg-items">
                        <!-- REDIRECTION TO User-Profile--><a class="chat-user" href="#"> <img class="circle" src="{{ URL::asset('public/theme/images/placeholder/300x300t.jpg') }}" alt=""><span class="chat-status with-badge online">10</span></a>
                        <!-- Redirection to msg/chat--><a class="chat-content" href="#"><span class="title">Jenny Jordan</span>
                          <div class="notify-desc">
                             Hey, How are you doing?</div><span class="chat-time">30 Minutes ago.</span></a>
                      </li>
                      <li class="collection-item msg-items">
                        <!-- REDIRECTION TO User-Profile--><a class="chat-user" href="#"> <img class="circle" src="{{ URL::asset('public/theme/images/placeholder/300x300g.jpg') }}" alt=""><span class="chat-status with-badge offline">5</span></a>
                        <!-- Redirection to msg/chat--><a class="chat-content" href="#"><span class="title">Jack Jordan</span>
                          <div class="notify-desc">
                             Wish you Happy birthday!</div><span class="chat-time">20 Minutes ago.</span></a>
                      </li>
                      <li class="collection-item msg-items"><a class="chat-user" href="#"> <img class="circle" src="{{ URL::asset('public/theme/images/placeholder/300x300t.jpg') }}" alt=""><span class="chat-status with-badge away">10</span></a><a class="chat-content" href="#"><span class="title">Selena Morris</span>
                          <div class="notify-desc">
                             Envato is really awesome place to be.</div><span class="chat-time">15 Minutes ago.</span></a></li>
                      <li class="collection-item msg-items"><a class="chat-user" href="#"> <img class="circle" src="{{ URL::asset('public/theme/images/placeholder/300x300g.jpg') }}" alt=""><span class="chat-status with-badge busy">10</span></a><a class="chat-content" href="#"><span class="title">Mira Dellon</span>
                          <div class="notify-desc">
                             Let meet up at my place in this weekends.</div><span class="chat-time">10 Minutes ago.</span></a></li>
                      <li class="collection-item msg-items"><a class="chat-user" href="#"> <img class="circle" src="{{ URL::asset('public/theme/images/placeholder/300x300t.jpg') }}" alt=""><span class="chat-status with-badge online">10</span></a><a class="chat-content" href="#"><span class="title">Evi Willson</span>
                          <div class="notify-desc">
                             Are you ready for party on this weekend?</div><span class="chat-time">5 Minutes ago.</span></a></li>
                    </ul>
                  </li>
                  <li class="collection-item msg-footer pos-relative"><a class="center tooltipped" href="#" data-position="top" data-tooltip="See all messages" style="padding:7px 16px;"><i class="material-icons small clear-style">more_horiz</i></a></li>
                </ul>
                <ul class="collection dropdown-content" id="top-nav-userProfile">
                  <li class="collection-item">
                    <div class="admin-profile-content"><img class="circle user-profile-img" src="{{ URL::asset('public/theme/images/placeholder/50x50g.jpg') }}" alt="">
                      <p class="user-name primary-text">{{ Auth::user()->name }}</p>
                      <p class="user-designation secondary-text">{{ Auth::user()->email }}</p>
                      <div class="divider"></div>
                      <ul class="profile-ul">
                        <li class="profile-li"><a class="btn waves-light collection-item" href="#"><i class="material-icons left">settings</i><span class="text-items">Profile</span></a></li>
                        <li class="profile-li"><a class="btn waves-light collection-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="material-icons left">power_settings_new</i><span class="text-items">Logout</span></a></li>
                      </ul>
                    </div>
                  </li>
                </ul>
              </div>
              <!-- Searchbar-->
              <form class="inactive animated" id="app-search">
                <div class="input-field">
                  <input type="search" id="search">
                  <label class="label-icon" for="search"><i class="material-icons">search</i></label><i class="material-icons app-search-Cls">close</i>
                </div>
              </form>
            </div>
          </nav>
        </div>
      </div>
      <!-- ============================-->
      <!-- Vertical Navigation (Default and Iconized)-->
      <!-- ============================-->
      <div class="vertical-navigations animated">
        <ul class="side-nav fixed animated collapsible collapsible-accordion" id="nav-default">
          <li class="logo"><a class="brand-logo hide-on-large-only" id="logo-container" href="/"></a></li>
          <li class="usr-profile">
            <div class="usr-profile-header"><a href="#"><img class="circle" src="{{ URL::asset('public/theme/images/placeholder/50x50g.jpg') }}" alt="Thor"></a></div>
            <ul class="user-options">
              <li class="waves-effect waves-set"><span class="usr-name">{{ Auth::user()->name }}</span></li>
              <li class="user-option-item waves-effect waves-set"><a class="btn-floating btn-small waves-effect waves-light" href="#"><i class="material-icons">settings</i></a></li>
              <li class="user-option-item waves-effect waves-set"><a class="btn-floating btn-small waves-effect waves-light" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="material-icons">power_settings_new</i></a></li>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </ul>
          </li>
          <li><a class="collapsible-header no-col-body waves-effect waves-set active current" href="{{ URL('home') }}"><i class="material-icons">dashboard</i><span>Dashboard</span></a></li>
          <li class="navigation-header"><span class="no-col-body">Management</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="CSS Properties">more_horiz</i>
          </li>
          <li><a class="collapsible-header waves-effect waves-set" href="Javascript:void(0);"><i class="material-icons">card_giftcard</i><span>Users</span><i class="material-icons mdi-navigation-chevron-left">keyboard_arrow_left</i></a>
            <div class="collapsible-body">
              <ul>
                <li class="menu-item"><a class="waves-effect waves-set" href="{{ URL('users') }}"><span>View Users</span></a></li>
                <li class="menu-item"><a class="waves-effect waves-set" href="{{ URL('users/add') }}"><span>Add User</span></a></li>
              </ul>
            </div>
          </li>
        </ul>
      </div>
     
    </header>