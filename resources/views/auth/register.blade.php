<!DOCTYPE html>
<html class="loading" lang="en">
  <!-- Item Name: Forge - Material Design Admin Template
  Version: 1.0
  Author: CodigoForge
  Author URL: https://themeforest.net/user/codigoforge
  WebSite : http://www.codigoforge.com
  -->
  <head>
  <link rel="icon" href="{{ URL::asset('public/theme/images/favicon/favicon-32x32.png') }}" sizes="32x32">
    <link rel="apple-touch-icon-precomposed" href="{{ URL::asset('public/theme/images/favicon/favicon-32x32.png') }}">
    <!-- ============================-->
    <!-- META DATA-->
    <!-- ============================-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="msapplication-TileColor" content="#FFFFFF">
    <meta name="msapplication-TileImage" content="{{ URL::asset('public/theme/images/favicon/favicon-32x32.png') }}">
    <meta name="theme-color" content="#2a56c6">
    <!-- ============================-->
    <!-- TITLE-->
    <!-- ============================-->
    <title>Adwords | SignUp</title>
    <!-- ============================-->
    <!-- FONTS-->
    <!-- ============================-->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Inconsolata" type="text/css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/icon?family=Material+Icons">
    <!-- ============================-->
    <!-- CSS-->
    <!-- ============================-->
    <link rel="stylesheet" href="{{ URL::asset('public/theme/css/main.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('public/theme/css/dynamic.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('public/theme/css/markup.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('public/theme/plugins/scrollbar/perfect-scrollbar.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('public/theme/css/custom.css') }}">
  </head>
  <body class="signin">
    <!-- ============================-->
    <!-- CONTENT AREA-->
    <!-- ============================-->
    <div class="signup-wrapper auth-wrap transparent">
      <div class="signup-form card-dash grey-transparent">
      <div class="card-header primary-bg z-depth-2"><a class="animated app-logo" href="javascript:void(0)"><span class="left" style="margin-left:25px;">ADWORDS REGISTRATION</span></a></div>
        <div class="row">
          <form method="POST" class="col s12" action="{{ route('register') }}">
            @csrf
            <div class="row">
                <div class="input-field s12 col"><i class="material-icons prefix">person</i>
                <input id="name" type="text" class="validate black-text{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                    <label for="name">Name</label>
                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
                
              <div class="input-field col s12"><i class="material-icons prefix">person</i>
                <input id="email" type="email" class="validate black-text lowercase-text{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                <label for="email">Email</label>
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
              </div>
              <div class="input-field col s12"><i class="material-icons prefix">vpn_key</i>
                <input id="sign-password" type="password" class="validate black-text{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                <label for="sign-password">Password</label>
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
              </div>
              <div class="input-field col s12"><i class="material-icons prefix">vpn_key</i>
                <input id="password-confirm" type="password" class="validate black-text" name="password_confirmation" required>
                <label for="password-confirm">Confirm Password</label>
              </div>
              <div class="input-field col s12">
                <input class="cb-secondary" type="checkbox" id="terms" required>
                <label for="terms">Agree to terms & conditions?</label>
              </div>
              <div class="input-field col s12 center">
                <button class="btn waves-effect waves-light sigin-submit" type="submit" name="login">{{ __('Register') }}<i class="material-icons right white-text">send</i></button>
              </div>
              <div class="col s12 center nav-link"><a class="switchVisibility" href="{{ URL('login') }}" data-ref="signin-wrapper">Already have an account?</a></div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- ============================-->
    <!-- SCRIPTS-->
    <!-- ============================-->
    <script type="text/javascript" src="{{ URL::asset('public/theme/js/all.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('public/theme/js/signin.js') }}"></script>
  </body>
</html>