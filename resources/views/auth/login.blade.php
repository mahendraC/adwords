<!DOCTYPE html>
<html class="loading" lang="en">
<!-- Item Name: Forge - Material Design Admin Template
Version: 1.0
Author: CodigoForge
Author URL: https://themeforest.net/user/codigoforge
WebSite : http://www.codigoforge.com
-->
<head>
    <link rel="icon" href="{{ URL::asset('public/theme/images/favicon/favicon-32x32.png') }}" sizes="32x32">
    <link rel="apple-touch-icon-precomposed" href="{{ URL::asset('public/theme/images/favicon/favicon-32x32.png') }}">
    <!-- ============================-->
    <!-- META DATA-->
    <!-- ============================-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="msapplication-TileColor" content="#FFFFFF">
    <meta name="msapplication-TileImage" content="{{ URL::asset('public/theme/images/favicon/favicon-32x32.png') }}">
    <meta name="theme-color" content="#2a56c6">
    <!-- ============================-->
    <!-- TITLE-->
    <!-- ============================-->
    <title>Adwords | SignIn</title>
    <!-- ============================-->
    <!-- FONTS-->
    <!-- ============================-->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Inconsolata" type="text/css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/icon?family=Material+Icons">
    <!-- ============================-->
    <!-- CSS-->
    <!-- ============================-->
    <link rel="stylesheet" href="{{ URL::asset('public/theme/css/main.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('public/theme/css/dynamic.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('public/theme/css/markup.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('public/theme/plugins/scrollbar/perfect-scrollbar.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('public/theme/css/custom.css') }}">
</head>
<body class="signin">
<!-- ============================-->
<!-- CONTENT AREA-->
<!-- ============================-->
<div class="signin-wrapper auth-wrap transparent">
    <div class="signup-form card-dash grey-transparent">
        <div class="card-header primary-bg z-depth-2"><a class="animated app-logo" href="javascript:void(0)"><span class="left" style="margin-left:25px;">ADWORDS LOGIN</span></a></div>
        <div class="row">
            <form class="col s12" method="post" action="{{ route('login') }}">
            @csrf
                <div class="row">
                    <div class="input-field col s12"><i class="material-icons prefix">person</i>
                        <input id="email" type="email" class="validate black-text lowercase-text{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                        <label for="email">Email</label>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="input-field col s12"><i class="material-icons prefix">vpn_key</i>
                        <input id="password" type="password" class="validate black-text{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                        <label for="password">Password</label>
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="input-field col s6 no-mrpd">
                    <input class="cb-secondary" type="checkbox" name="remember" id="remember_me" {{ old('remember') ? 'checked' : '' }}>
                        <label for="remember_me">Remember Me</label>
                    </div>
                    <div class="input-field col s6 no-mrpd right-align">
                        <label class="forgot-password" for="label-label"><a class="grey-text" href="#">forgot password?</a></label>
                    </div>
                    <div class="input-field col s12 center">
                        <button class="btn mm-btn waves-effect waves-light sigin-submit" type="submit" name="login" data-preloader="blue" data-text="Login" data-icon="send" data-redirection="home.html">Login<i class="material-icons right white-text">send</i></button>
                    </div>
                    <div class="col s12 center nav-link"><a class="switchVisibility" href="{{ URL('register') }}" data-ref="signup-wrapper">Create account</a></div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- ============================-->
<!-- SCRIPTS-->
<!-- ============================-->
<script type="text/javascript" src="{{ URL::asset('public/theme/js/all.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('public/theme/js/signin.js') }}"></script>
</body>
</html>
